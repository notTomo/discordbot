const fetch = require('node-fetch')
const Discord = require('discord.js');
const { progname, authkey } = require('./config.json');


module.exports = {
    name: "comp",

    async run (client, message, args) {

    let key = args[0]
    
    let hours = args[1]


    if(!authkey) return message.channel.send(new Discord.MessageEmbed().setDescription(`The \`AuthKey\` **Has Not Been Set!**\n In Order To Use This Bot You Must Edit Your Config`).setColor("RED").setTimestamp());

    if(!key) return message.channel.send('Please Provide A Valid Key');

    if(!hours) return message.channel.send('Please Provide A Valid Amount Of Hours');

    if(!progname) return message.channel.send('Please Check Your Config');

        try {
            await fetch(`https://panel.proxine.ninja/api/apiaccess.php?api=${authkey}&action=comp&program=${progname}&hours=${hours}&key=${key}`)
                .then(res => res.json())

                .then(json => {


                   let embed = new Discord.MessageEmbed()
                   .setTitle('Key Successfully Resetted!')
                   .addField('Resetted By:', message.author)
                   .addField('Key Resetted:', `\`${key}\``)
                   .setColor("ORANGE")
                   .setTimestamp()
                   message.channel.send(embed)

                })

        } catch (error) {
            message.channel.send(`**ERROR:** \`${error}\``)
        }
    }
}