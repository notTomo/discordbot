const fetch = require('node-fetch')
const Discord = require('discord.js');
const { progname, authkey } = require('.././config.json');


module.exports = {
    name: "keyinfo",

    async run(client, message, args) {

        let key = args[0]

        if (!authkey) return message.channel.send(new Discord.MessageEmbed().setDescription(`The \`AuthKey\` **Has Not Been Set!**\n In Order To Use This Bot You Must Edit Your Config`).setColor("RED").setTimestamp());

        if (!key) return message.channel.send('Please Provide A Valid Key');

        if (!progname) return message.channel.send('Please Check Your Config');

        try {
            await fetch(`https://panel.proxine.ninja/api/apiaccess.php?api=${authkey}&action=keyinfo&program=${progname}&key=${key}`)
                .then(res => res.json())

                .then(json => {

                    let arr = [ json["key"], json["hwid"], json["expiry"], json["level"] ];
                    let check = true;

                    arr.forEach(item => {
                        if (item == undefined || item == null) {
                            check = false;
                        }
                    })

                    if (check) {
                        let embed = new Discord.MessageEmbed()
                            .setTitle('Key Informations Generated!')
                            .addField('Requested By:', message.author)
                            .addField('Key:', `\`${json["key"]}\``, true)
                            .addField('HWID:', `\`${json["hwid"]}\``, true)
                            .addField('Expiry on:', `\`${json["expiry"]}\``, true)
                            .addField('Level:', `\`${json["level"]}\``, true)
                            .setColor("#11d1cb")
                            .setTimestamp()
                        message.channel.send(embed)
                    } else {
                        let embed = new Discord.MessageEmbed()
                            .setTitle('Generation Error!')
                            .setDescription("**The Key Is Invalid!**")
                            .setColor("#ff0000")
                            .setTimestamp()
                        message.channel.send(embed)
                    }


                })

        } catch (error) {
            message.channel.send(`**ERROR:** \`${error}\``)
        }
    }
}









