const fetch = require('node-fetch')
const Discord = require('discord.js');
const { progname, authkey } = require('.././config.json');


module.exports = {
    name: "del",

    async run(client, message, args) {

        let key = args[0]


        if (!authkey) return message.channel.send(new Discord.MessageEmbed().setDescription(`The \`AuthKey\` **Has Not Been Set!**\n In Order To Use This Bot You Must Edit Your Config`).setColor("RED").setTimestamp());

        if (!key) return message.channel.send('Please Provide A Valid Key');

        if (!progname) return message.channel.send('Please Check Your Config');

        try {
            await fetch(`https://panel.proxine.ninja/api/apiaccess.php?api=${authkey}&action=delete&program=${progname}&key=${key}`)
                .then(res => res.json())

                .then(json => {

                    if (key == undefined || key == null) {
                        let embed = new Discord.MessageEmbed()
                            .setTitle('Error!')
                            .setDescription("**The Key Is Invalid!**")
                            .setColor("#ff0000")
                            .setTimestamp()
                        message.channel.send(embed)
                    } else {
                        let embed = new Discord.MessageEmbed()
                            .setTitle('Key Successfully Deleted!')
                            .addField('Deleted By:', message.author)
                            .addField('Key Deleted:', `\`${key}\``)
                            .setColor("RED")
                            .setTimestamp()
                        message.channel.send(embed)
                    }

                })

        } catch (error) {
            message.channel.send(`**ERROR:** \`${error}\``)
        }
    }
}