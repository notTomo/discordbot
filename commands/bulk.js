const fetch = require('node-fetch')
const Discord = require('discord.js');
const { progname, authkey } = require('.././config.json');


module.exports = {
    name: "bulk",

    async run(client, message, args) {

        let hours = parseInt(args[0])
        let level = parseInt(args[1])
        let amount = parseInt(args[2])

        if (!authkey) return message.channel.send(new Discord.MessageEmbed().setDescription(`The \`AuthKey\` **Has Not Been Set!**\n In Order To Use This Bot You Must Edit Your Config`).setColor("RED").setTimestamp());

        if (!hours) return message.channel.send('Please Provide A Valid Amount Of Hours');

        if (!level) return message.channel.send('Please Provide A Valid Key Level');

        if (!amount) return message.channel.send('Please Provide A Valid Amount Of Key');

        if (!progname) return message.channel.send('Please Check Your Config');



        try {
            await fetch(`https://panel.proxine.ninja/api/apiaccess.php?api=${authkey}&action=bulk&type=${hours}&program=${progname}&level=${level}&amount=${amount}`)
                .then(res => res.text())

                .then(body => {

                    let j = 0;
                    let arr = "";

                    for (let i in body) {
                        if (body[i] == "<" || body[i] == "b" || body[i] == "r" || body[i] == "/" || body[i] == ">") break;
                        if (j < 29) {
                            arr = arr + body[i];
                            j++;
                        } else {
                            j = 0;
                        }
                        
                    }
                    
                    console.log(arr);
                    let embed = new Discord.MessageEmbed()
                        .setTitle('Keys Generated!')
                        .addField('Requested By:', message.author, true)
                        .addField('Keys Generated:', `\`${body.replace('<br />', '')}\``, true)
                        .setColor("PURPLE")
                        .setTimestamp()
                    message.channel.send(embed)

                })

        } catch (error) {
            message.channel.send(`**ERROR:** \`${error}\``)
        }
    }
}