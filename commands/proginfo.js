const fetch = require('node-fetch')
const Discord = require('discord.js');
const { progname, authkey } = require('.././config.json');


module.exports = {
    name: "proginfo",

    async run (client, message, args) {        
        
    if(!authkey) return message.channel.send(new Discord.MessageEmbed().setDescription(`The \`AuthKey\` **Has Not Been Set!**\n In Order To Use This Bot You Must Edit Your Config`).setColor("RED").setTimestamp());


    if(!progname) return message.channel.send('Please Check Your Config');

        try {
            await fetch(`https://panel.proxine.ninja/api/apiaccess.php?api=${authkey}&action=programinfo&program=${progname}`)
                .then(res => res.json())
                
                .then(json => {
                   
                   let embed = new Discord.MessageEmbed()
                   .setTitle('Program Informations Generated!')
                   .addField('Requested By:', message.author, true)
                   .addField('Total Key:', `\`${json["total program keys"]}\``, true)
                   .setColor("PURPLE")
                   .setTimestamp()
                   message.channel.send(embed)

                })

        } catch (error) {
            message.channel.send(`**ERROR:** \`${error}\``)
        }
    }
}